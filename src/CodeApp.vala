public class CodeApp : Gtk.Application {
    public CodeApp() {
        Object(
            application_id: "com.neineafactory.codeapp",
            flags: ApplicationFlags.FLAGS_NONE
        );
    } 

    protected override void activate() {
        this.new_window();
    }

    public MainWindow new_window() {
        return new MainWindow(this);
    }

    public static int main(string[] args) {
        return new CodeApp().run(args);
    }
}
