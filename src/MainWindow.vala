public class MainWindow : Gtk.ApplicationWindow {
    private EditorView editor_view;

    public MainWindow(CodeApp app) {
        Object(
            application: app,
            title: "CodeApp"
        );
    }

    construct {
        this.set_show_menubar(true);
        this.maximize();
        editor_view = new EditorView();
        this.set_child(editor_view);
        this.present();
    }
}
