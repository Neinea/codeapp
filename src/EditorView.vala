public class EditorView : GtkSource.View {
    public EditorView() {
        Object();
    }
    
    construct {
        this.set_show_line_numbers(true);
    }
}
